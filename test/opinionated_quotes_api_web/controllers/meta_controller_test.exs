defmodule OpinionatedQuotesApiWeb.MetaControllerTest do
  use OpinionatedQuotesApiWeb.ConnCase

  describe "GET v1_meta_path" do
    test "gets # of quotes only as default", %{conn: conn} do
      response =
        get(conn, v1_meta_path(conn, :get_metadata))
        |> json_response(200)

      assert Map.get(response, "n_quotes", -1) >= 0
      assert map_size(response) == 1
    end

    test "gets # of quotes only", %{conn: conn} do
      response =
        get(conn, v1_meta_path(conn, :get_metadata, ks: "n_quotes"))
        |> json_response(200)

      assert Map.get(response, "n_quotes", -1) >= 0
      assert map_size(response) == 1
    end

    test "gets the maximum possible # of quotes for the response", %{conn: conn} do
      response =
        get(conn, v1_meta_path(conn, :get_metadata, ks: "max_quotes"))
        |> json_response(200)

      assert Map.get(response, "max_quotes", -1) > 0
      assert map_size(response) == 1
    end

    test "gets several fields, case-insensitive, white-spaces allowed", %{conn: conn} do
      response =
        get(conn, v1_meta_path(conn, :get_metadata, ks: "  max_Quotes,  n_quOTes"))
        |> json_response(200)

      assert Map.get(response, "n_quotes", -1) >= 0
      assert Map.get(response, "max_quotes", -1) > 0
      assert map_size(response) == 2
    end

    test "returns an error on an unexpected key", %{conn: conn} do
      response =
        get(conn, v1_meta_path(conn, :get_metadata, ks: "routes"))
        |> json_response(400)

      assert response["error"] =~ "An unexpected key or delimiter (expecting ',') in 'ks' query parameter"
      assert map_size(response) == 1
    end

    test "returns an error on an unexpected key among legitible keys", %{conn: conn} do
      response =
        get(conn, v1_meta_path(conn, :get_metadata, ks: "n_quotes,routes,max_quotes"))
        |> json_response(400)

      assert response["error"] =~ "An unexpected key or delimiter (expecting ',') in 'ks' query parameter"
      assert map_size(response) == 1
    end
  end
end
