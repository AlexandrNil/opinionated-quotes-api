defmodule OpinionatedQuotesApiWeb.PageControllerTest do
  use OpinionatedQuotesApiWeb.ConnCase

  describe "index page" do
    test "GET /", %{conn: conn} do
      conn = get conn, "/"
      html = html_response(conn, 200)

      assert html =~ "Routes"
      assert html =~ "tmode"
      assert html =~ "/meta"
      assert html =~ "charities"
      refute String.match?(html, ~r/todo/i)
    end
  end
end
