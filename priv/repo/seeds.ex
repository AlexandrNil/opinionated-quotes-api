defmodule SeedTask do
  alias OpinionatedQuotesApi.QuoteAPI.Quote

  def seed_quotes_from_json() do
    Path.join(["#{:code.priv_dir(:opinionated_quotes_api)}", "repo", "quotes.json"])
    |> seed_quotes_from_json()
  end
  def seed_quotes_from_json(flname) do
    get_quotes(flname)
    |> Enum.each(&(
      Quote.insert_or_update_quote_with_tags(%Quote{}, &1)
    ))
  end

  defp get_quotes(flname) do
    File.read!(flname)
    |> Poison.decode!
    |> Map.fetch!("quotes")
  end
end
