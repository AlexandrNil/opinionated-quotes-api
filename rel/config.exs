# Import all plugins from `rel/plugins`
# They can then be used by adding `plugin MyPlugin` to
# either an environment, or release definition, where
# `MyPlugin` is the name of the plugin module.
Path.join(["rel", "plugins", "*.exs"])
|> Path.wildcard()
|> Enum.map(&Code.eval_file(&1))

use Mix.Releases.Config,
    # This sets the default release built by `mix release`
    default_release: :default,
    # This sets the default environment used by `mix release`
    default_environment: Mix.env()

# For a full list of config options for both releases
# and environments, visit https://hexdocs.pm/distillery/configuration.html


# You may define one or more environments in this file,
# an environment's settings will override those of a release
# when building in that environment, this combination of release
# and environment configuration is called a profile

environment :dev do
  # If you are running Phoenix, you should make sure that
  # server: true is set and the code reloader is disabled,
  # even in dev mode.
  # It is recommended that you build with MIX_ENV=prod and pass
  # the --env flag to Distillery explicitly if you want to use
  # dev mode.
  set dev_mode: true
  set include_erts: false
  set cookie: :"U>8a1<j[><]18v|Bi}?,Z$fZA]hD!RIndq~B6R4T3eUE.WJ4A(!E_rM=z&`w4aKW"
end

environment :prod do
  set include_erts: true
  set include_src: false
  set cookie: :"T6&h`16qzNVBw(<s,wqYuY?kzZKP0{1mzqGrsOh<PI)Iu_ZAN}T}{dPTikC,v[~/"
end

# You may define one or more releases in this file.
# If you have not set a default release, or selected one
# when running `mix release`, the first release in the file
# will be used by default

release :opinionated_quotes_api do
  set version: current_version(:opinionated_quotes_api)
  set applications: [
    :runtime_tools
  ]
  set commands: [
    "seed": "rel/commands/seed.sh"
  ]
end
