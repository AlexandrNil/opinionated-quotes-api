defmodule OpinionatedQuotesApi.Application do
  use Application

  @quote_cnt_cache_name :quote_cnt

  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do
    import Supervisor.Spec

    :ets.new(@quote_cnt_cache_name, [:named_table, :public, read_concurrency: true])
    :ets.insert(
      @quote_cnt_cache_name,
      [{:updated_at, 111}, {:quote_cnt, 0}]
    )

    # Define workers and child supervisors to be supervised
    children = [
      # Start the Ecto repository
      supervisor(OpinionatedQuotesApi.Repo, []),
      # Start the endpoint when the application starts
      supervisor(OpinionatedQuotesApiWeb.Endpoint, []),
      # Start your own worker by calling: OpinionatedQuotesApi.Worker.start_link(arg1, arg2, arg3)
      # worker(OpinionatedQuotesApi.Worker, [arg1, arg2, arg3]),
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: OpinionatedQuotesApi.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    OpinionatedQuotesApiWeb.Endpoint.config_change(changed, removed)
    :ok
  end

  def stop(_state) do
    # this is probably totally unnecessary
    :ets.delete(@quote_cnt_cache_name)
  end
end
