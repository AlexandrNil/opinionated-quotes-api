defmodule OpinionatedQuotesApiWeb.QuoteParamsHelper do
  alias OpinionatedQuotesApi.Helpers.Sanitizer
  alias OpinionatedQuotesApi.QuoteAPI.Tag

  @n_max_re ~r/^max/i
  @true_vals_for_rand MapSet.new(["t", "", "true", "1", nil])
  @max_n Application.get_env(
    :opinionated_quotes_api, OpinionatedQuotesApiWeb.Endpoint
  )[:max_quotes_in_response]

  def prepare_params_4_query(params) do
    with {n} <- parse_n(Map.get(params, "n", "1")),
      {offset} <- (case Integer.parse( Map.get(params, "offset", "0") ) do
        {num, _} ->
          {(if num >= 0, do: num, else: 0)}
        :error ->
          {:error, "'offset' query parameter should be a non-negative integer if present."}
      end),
      {tagmode} <- parse_tagmode(params["tmode"])
    do
      rand =
        if MapSet.member?(@true_vals_for_rand, Map.get(params, "rand", "t")), do: true, else: false
      author =
        params["author"] && String.trim(params["author"]) |> Sanitizer.sanitize_sql_like()
      tags =
        Tag.parse_all(params["tags"] || "")
      lang = params["lang"]
        && String.trim(params["lang"])
        |> String.downcase()
        |> Sanitizer.sanitize_sql_like()

      [rand: rand, n: n, offset: offset, author: author, tags: tags, tagmode: tagmode, lang: lang]
    else
      err_tup -> err_tup
    end
  end

  defp parse_n(raw_n) do
    if Regex.match?(@n_max_re, raw_n), do: {@max_n}, else: (case Integer.parse(raw_n) do
      {num, _} ->
        {(if num > 0, do: num, else: 1)}
      :error ->
        {:error, "'n' query parameter (if present) should be a positive integer or 'max'."}
    end)
  end

  defp parse_tagmode(nil), do: {"all"}
  defp parse_tagmode(raw) do
    case String.downcase(raw) do
      "any" = s -> {s}
      "all" = s -> {s}
      _ -> {:error, "'tmode' query parameter (if present) should be 'all' or 'any'."}
    end
  end
end
