defmodule OpinionatedQuotesApiWeb.V1.QuoteController do
  use OpinionatedQuotesApiWeb, :controller
  alias OpinionatedQuotesApi.QuoteAPI.QuoteAPI
  alias OpinionatedQuotesApiWeb.QuoteParamsHelper

  def get_quotes(conn, params) do
    num_reqs =
      Application.get_env(
        :opinionated_quotes_api, OpinionatedQuotesApiWeb.Endpoint
      )[:rate_limiting][:quotes][:num_reqs]
    per_ms =
      Application.get_env(
        :opinionated_quotes_api, OpinionatedQuotesApiWeb.Endpoint
      )[:rate_limiting][:quotes][:per_ms]

    case Hammer.check_rate("get_quotes:#{:inet.ntoa(conn.remote_ip) |> to_string()}", per_ms, num_reqs) do
      {:allow, _cnt} ->
        case QuoteParamsHelper.prepare_params_4_query(params) do
          kw_params when is_list(kw_params) ->
            json(conn, %{quotes: QuoteAPI.list_quotes(kw_params)})
          {:error, msg} ->
            put_status(conn, 400)
            |> json(%{error: msg})
        end

      {:deny, _lim} ->
        put_status(conn, 400)
        |> json(%{error: "Too frequent requests"})
    end
  end
end
