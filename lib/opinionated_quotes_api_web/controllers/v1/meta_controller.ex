defmodule OpinionatedQuotesApiWeb.V1.MetaController do
  use OpinionatedQuotesApiWeb, :controller
  alias OpinionatedQuotesApi.QuoteAPI.QuoteAPI

  @allowed_metakeys MapSet.new(["n_quotes", "max_quotes"])
  @max_quotes Application.get_env(
    :opinionated_quotes_api, OpinionatedQuotesApiWeb.Endpoint
  )[:max_quotes_in_response]

  def get_metadata(conn, params) do
    num_reqs =
      Application.get_env(
        :opinionated_quotes_api, OpinionatedQuotesApiWeb.Endpoint
      )[:rate_limiting][:meta][:num_reqs]
    per_ms =
      Application.get_env(
        :opinionated_quotes_api, OpinionatedQuotesApiWeb.Endpoint
      )[:rate_limiting][:meta][:per_ms]

    with {:allow, _cnt} <- Hammer.check_rate("get_metadata:#{:inet.ntoa(conn.remote_ip) |> to_string()}", per_ms, num_reqs),
      {:ok, resp_data} <- prepare_resp_data(
        Map.get(params, "ks") |> parse_raw_ks
      )
    do
      json(conn, resp_data)
    else
      {:error, msg} ->
        put_status(conn, 400)
        |> json(%{error: msg})

      {:deny, _lim} ->
        put_status(conn, 400)
        |> json(%{error: "Too frequent requests"})
    end
  end

  defp prepare_resp_data(ks) do
    if MapSet.subset?(ks, @allowed_metakeys) do
      resp_data = if MapSet.member?(ks, "max_quotes") do
        %{max_quotes: @max_quotes}
      else
        %{}
      end

      {:ok, if MapSet.member?(ks, "n_quotes") or MapSet.size(ks) == 0 do
        Map.put(resp_data, "n_quotes", QuoteAPI.get_recent_quote_count)
      else
        resp_data
      end}
    else
      {:error, "An unexpected key or delimiter (expecting ',') in 'ks' query parameter."}
    end
  end

  defp parse_raw_ks(nil), do: MapSet.new()
  defp parse_raw_ks(raw_ks) do
    String.split(raw_ks, ",")
    |> Enum.map( &(String.trim(&1) |> String.downcase) )
    |> MapSet.new
  end
end
